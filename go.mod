module gitlab.com/redfield/usbctl

require (
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/golang/protobuf v1.2.0
	github.com/google/gousb v0.0.0-20190125150036-d0c05ab7f70d
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	google.golang.org/grpc v1.18.0
)
